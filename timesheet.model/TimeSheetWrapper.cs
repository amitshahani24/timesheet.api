﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class TimeSheetWrapper
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public List<TimeSheetByDayWrapper> TimeSheets { get; set; }
    }
}
