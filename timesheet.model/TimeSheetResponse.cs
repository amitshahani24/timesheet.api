﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class TimeSheetResponse
    {
        public int EmployeeId { get; set; }
        public List<TimeSheetWrapper> TimesheetsByTask { get; set; }
    }
}
