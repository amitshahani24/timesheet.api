﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class EmployeeSummaryModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public double WeeklyTotalHours { get; set; }
        public double WeeklyAvgHours { get; set; }
    }
}
