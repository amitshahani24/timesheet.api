﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class TimeSheetByDayWrapper
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public DateTime BusinessDate { get; set; }
        public double Hours { get; set; }
    }
}
