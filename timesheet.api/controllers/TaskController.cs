﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/Task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        TaskService taskService;
        public TaskController(TaskService taskService)
        {
            this.taskService = taskService;
        }
        [Route("getall")]
        [HttpGet]
        public IActionResult GetAllTasks()
        {
            var tasks = taskService.GetAllTasks();
            return new ObjectResult(tasks);
        }
    }
}