﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/Timesheet")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly TimesheetService timesheetService;
        public TimesheetController(TimesheetService timesheetService)
        {
            this.timesheetService = timesheetService;
        }

        [HttpGet]
        public IActionResult GetAll(int employeeId,DateTime fromDate, DateTime toDate)
        {
            var items = this.timesheetService.GetTimesheets(employeeId, fromDate, toDate);
            return new ObjectResult(items);
        }

        [HttpPost]
        public IActionResult SaveTimesheet(Timesheet timesheet)
        {
            this.timesheetService.SaveTimesheets(timesheet);
            return new ObjectResult(null);
        }
    }
}