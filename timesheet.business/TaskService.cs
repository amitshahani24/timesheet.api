﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
   public class TaskService
    {
        TimesheetDb db;
        public TaskService(TimesheetDb timesheetDb)
        {
            this.db = timesheetDb;
        }

        public IQueryable<Task> GetAllTasks()
        {
            return this.db.Tasks;
        }
    }
}
