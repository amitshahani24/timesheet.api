﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TimesheetService
    {
        TimesheetDb db;
        public TimesheetService(TimesheetDb context)
        {
            this.db = context;
        }

        public TimeSheetResponse GetTimesheets(int employeeId, DateTime start, DateTime end)
        {
            var employeeTimeSheets = this.db.TimeSheets.Where(t => t.EmployeeId == employeeId).ToList();
            var tasks = this.db.Tasks.Where(t => employeeTimeSheets.Select(s => s.TaskId).Contains(t.Id)).ToList();
            var dates = Enumerable.Range(0, 1 + end.Subtract(start).Days)
          .Select(offset => start.AddDays(offset))
          .ToArray();
            TimeSheetResponse timeSheetResponse = new TimeSheetResponse();
            timeSheetResponse.EmployeeId = employeeId;
            timeSheetResponse.TimesheetsByTask = (
                                                  from d in dates
                                                  from tsk in tasks
                                                  join t in employeeTimeSheets on new
                                                  {
                                                      businessDate = d,
                                                      taskId = tsk.Id
                                                  } equals new { businessDate = t.BusinessDate, taskId = t.TaskId }

                                                  into p
                                                  from ts in p.DefaultIfEmpty()


                                                  select new TimeSheetByDayWrapper()
                                                  {
                                                      BusinessDate = d,
                                                      Hours = ts == null ? 0 : ts.Hours,
                                                      TaskId = tsk.Id,
                                                      Id = ts == null ? -1 : ts.Id
                                                  })
                                                  .GroupBy(s => s.TaskId)
                                                  .Select(p => new TimeSheetWrapper()
                                                  {
                                                      TaskId = p.Key,

                                                      TimeSheets = p.ToList()

                                                  })

                                                  .ToList();

            foreach (var timesheet in timeSheetResponse.TimesheetsByTask)
            {
                timesheet.TaskName = tasks.First(s => s.Id == timesheet.TaskId).Name;
            }

            return timeSheetResponse;

        }

        private List<TimeSheetByDayWrapper> GetTimeSheetsByDay(List<Timesheet> timeSheet, int taskId, DateTime businessDate)
        {
            List<TimeSheetByDayWrapper> timesheetByDay = (from t in timeSheet
                                                          group t by t == null ? businessDate : t.BusinessDate into grp
                                                          select new TimeSheetByDayWrapper()
                                                          {

                                                          }).ToList();

            return timesheetByDay;
        }

        public void SaveTimesheets(Timesheet timesheet)
        {
            Timesheet timesheetObj = null;


            if (timesheet.Id > 0)
                timesheetObj = this.db.TimeSheets.Where(s => s.Id == timesheet.Id).FirstOrDefault();
            else
            {
                timesheetObj = this.db.TimeSheets.Where(s => s.BusinessDate == timesheet.BusinessDate
                                                  && s.EmployeeId == timesheet.EmployeeId
                                                  && s.TaskId == timesheet.TaskId).FirstOrDefault();

                if(timesheetObj != null)
                {
                    timesheetObj.Hours += timesheet.Hours;
                }

            }


            if (timesheetObj != null)
                UpdateTimeSheet(timesheetObj);
            else
                AddTimeSheet(timesheet);
            

            this.db.SaveChanges();

        }

        private void AddTimeSheet(Timesheet timesheet)
        {
            Timesheet timesheetObj = new Timesheet();
            timesheetObj.TaskId = timesheet.TaskId;
          
            timesheetObj.BusinessDate = timesheet.BusinessDate.Date;
            timesheetObj.EmployeeId = timesheet.EmployeeId;
            timesheetObj.Hours = timesheet.Hours;
            this.db.TimeSheets.Add(timesheetObj);

        }
        
        private void UpdateTimeSheet(Timesheet timesheet)
        {
            timesheet.TaskId = timesheet.TaskId;
            timesheet.BusinessDate = timesheet.BusinessDate.Date;
            timesheet.EmployeeId = timesheet.EmployeeId;
            timesheet.Hours = timesheet.Hours;
            
        }
    }
}
