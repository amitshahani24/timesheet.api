﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public IEnumerable<EmployeeSummaryModel> GetEmployeeSummary(DateTime fromDate, DateTime toDate)
        {
            int totalDays = (toDate - fromDate).Days + 1;
            var timesheets = (from t in this.db.TimeSheets
                              where t.BusinessDate >= fromDate
                              && t.BusinessDate <= toDate
                              select t);

            var result = (from e in this.db.Employees
                          join ts in timesheets on e.Id equals ts.EmployeeId
                          into p
                          from rs in p.DefaultIfEmpty()

                          group rs by new { e.Id, e.Code, e.Name } into grp
                          let list = grp.ToList()
                          select new EmployeeSummaryModel()
                          {
                              Id = grp.Key.Id,
                              Code = grp.Key.Code,
                              Name = grp.Key.Name,
                              WeeklyTotalHours =  list.Sum(s => s== null ? 0 : s.Hours),
                              WeeklyAvgHours = list.Sum(s => s == null ? 0 : s.Hours) / totalDays
                          }).ToList() ;

            return result;
        }
    }
}
